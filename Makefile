all: game.o testbuild.o client.o server.o
	g++ -g -Wall testbuild.o game.o -o test; g++ -g -Wall client.o -o client; rm client.o; g++ -g -Wall server.o game.o -o server; rm server.o 

client: client.o shelp.o
	g++ -g -Wall client.o shelp.o -o client

server: server.o game.o shelp.o
	g++ -g -Wall server.o game.o shelp.o -o server; rm server.o

server.o: server.cpp game.h shelp.h
	g++ -g -Wall -c server.cpp -I.

client.o: client.cpp shelp.h
	g++ -g -Wall -c client.cpp  -I.

shelp.o: shelp.h shelp.c
	g++ -g -Wall -c shelp.c -I.

testbuild.o: game.h testbuild.cpp
	g++ -g -Wall -c testbuild.cpp -I.


game.o: game.h game.cpp
	g++ -Wall -c game.cpp  -I.


clean:
	rm testbuild.o; rm game.o; rm client; rm server; rm test; rm server.o; rm client.o; rm shelp.o;
