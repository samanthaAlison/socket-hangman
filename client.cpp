#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include "shelp.h"
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>

char * strtolower(char * str) 
{
    int length = strlen(str);

    char * out = (char *) malloc(sizeof(char) * (length + 1));
    
    char * strIter = str;
    length = 0;
    while (*strIter != '\0')
    {
        *out = (char) tolower((unsigned char) *strIter);
        out++;
        strIter++;
        length++;
    }
    return (out - length);
} 

void get_sockaddr(int socketFd, struct sockaddr_in * addr)
{
    socklen_t len = sizeof(*addr);
    getsockname(socketFd, (struct sockaddr *) addr, &len);
}

in_port_t get_port(int socketFd)
{
    struct sockaddr_in addr;
    get_sockaddr(socketFd, &addr);

    return addr.sin_port;
}

int connect_udp(char * hostname, char * port) 
{
    int udpSocketFd;

    struct hostent * serverHost = gethostbyname(hostname);
    char * addrIpO = serverHost->h_addr_list[0];
    int * addrIp = (int *) serverHost->h_addr_list[0];// malloc(sizeof(unsigned int));
    in_addr_t serverIp = *addrIp;
    puts(serverHost->h_addr_list[0]);
    unsigned int i=0;
    printf("%s\n", inet_ntoa(*((in_addr * )&serverIp)));
    struct sockaddr_in serverAddr;
    memset(&serverAddr, '0', sizeof(serverAddr));
    serverAddr.sin_addr.s_addr = serverIp;
    serverAddr.sin_port = (in_port_t) htons(atoi(port));
    serverAddr.sin_family = AF_INET;

    printf("ip_add from udp serverr: %s", inet_ntoa(serverAddr.sin_addr));

    printf("port number of udp socket from server is: %i\n", (int) ntohs(serverAddr.sin_port));

    if ((udpSocketFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        puts("error creating socket\n");
        return -1;
    }

    struct sockaddr_in initAddr;
    initAddr.sin_family = AF_INET;
    initAddr.sin_addr.s_addr = INADDR_ANY;
#ifdef _SAM_TRACK_
    initAddr->sin_port = htons(0);
#else
    initAddr.sin_port = (in_port_t) htons(0);
#endif


    int opt = 1, result;
    //int result = setsockopt(udpSocketFd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    result = setsockopt(udpSocketFd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    if (bind(udpSocketFd, (struct sockaddr *)&initAddr, sizeof(initAddr)) == -1)
    {
        return -2;
    } 


    if (result)
    {
        int errsv = errno;
        printf("ERROR: %s", strerror(errsv));
        return -5;
    }
    socklen_t len = sizeof(serverAddr);
    /*result = connect(udpSocketFd, (struct sockaddr *) &serverAddr, len);
    if (result < 0) 
    {
        printf("Error: %i %s", errno, strerror(errno));
        return -1;
    }*/

    
    return udpSocketFd;
}

void start_udp(int udpSocketFd, char * hostname, char * port)
{
    struct hostent * serverHost = gethostbyname(hostname);
    char * addrIpO = serverHost->h_addr_list[0];
    int * addrIp = (int *) serverHost->h_addr_list[0];// malloc(sizeof(unsigned int));
    in_addr_t serverIp = *addrIp;
    puts(serverHost->h_addr_list[0]);
    unsigned int i=0;
    struct sockaddr_in serverAddr;
    memset(&serverAddr, '0', sizeof(serverAddr));
    serverAddr.sin_addr.s_addr = serverIp;
    serverAddr.sin_port = (in_port_t) htons(atoi(port));
    serverAddr.sin_family = AF_INET;
    socklen_t serverLen = sizeof(serverAddr);
    sockaddr_in sendAddr, recvAddr;
    get_sockaddr(udpSocketFd, &sendAddr);
    socklen_t recvLen = sizeof(recvAddr);
    socklen_t sendLen = sizeof(sendAddr);
    memset(&recvAddr, '0', recvLen);

    char readBuffer[1024];

    bool session = true; 

    while (session)
    {
        bool game = false;
         char sendBuffer[1024];
         fgets(sendBuffer, 1024, stdin);
        sendBuffer[strlen(sendBuffer) - 1] = '\0';
        hsend_udp(udpSocketFd, sendBuffer, 0, &serverAddr, serverLen);

        if (strcmp(sendBuffer, "ready") == 0) 
        {
            game = true;
        } else if (strcmp(sendBuffer, "bye") == 0) {
            session = false;
        }

        while (game)
        {
            memset(&recvAddr, '0', recvLen);
            hread_udp(udpSocketFd, readBuffer, 1024, 0,  &recvAddr, &recvLen);
            struct sockaddr * addr = (struct sockaddr *) &recvAddr;
            char command[1024], param[1024];
            split_read(readBuffer, command, param);
            if (strcmp(command, "bye") == 0) 
            {
                session = false;
                break;
            }
            else
            {
                puts(param);
                fgets(sendBuffer, 1024, stdin);
                sendBuffer[strlen(sendBuffer) - 1] = '\0';
                hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, sendLen);
            }
        }
    }
}

int connect_tcp(char * hostname, char * port) 
{

    int tcpSocketFd;

    struct hostent * serverHost = gethostbyname(hostname);
    char * addrIpO = serverHost->h_addr_list[0];
    int * addrIp = (int *) serverHost->h_addr_list[0];// malloc(sizeof(unsigned int));
    in_addr_t serverIp = *addrIp;
    puts(serverHost->h_addr_list[0]);
    unsigned int i=0;
    printf("%s\n", inet_ntoa(*((in_addr * )&serverIp)));
    struct sockaddr_in serverAddr;
    memset(&serverAddr, '0', sizeof(serverAddr));
    serverAddr.sin_addr.s_addr = serverIp;
    serverAddr.sin_port = (in_port_t) htons(atoi(port));
    serverAddr.sin_family = AF_INET;
   
    printf("ip_addr: %s", inet_ntoa(serverAddr.sin_addr));

    printf("port number of tcp socket is: %i\n", (int) ntohs(serverAddr.sin_port));

    /*addrinfo *aInfo = NULL;
    addrinfo *ap;

    int result = getaddrinfo(hostname, port, &hints, &aInfo);

    
    for (ap = aInfo; ap != NULL; ap = aInfo->ai_next) 
    {
        if ((tcpSocketFd = socket(ap->ai_family, ap->ai_socktype, ap->ai_protocol)) == -1)
        {
            continue;
        }



        if ((connect(tcpSocketFd, ap->ai_addr, ap->ai_addrlen)))
        {
            printf("port number of tcp socket is: %i\n", (int) (get_port(tcpSocketFd)));
            return tcpSocketFd;
        }


    }
    //return 0;*/

    if ((tcpSocketFd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        puts("error creating socket\n");
        return -1;
    }
    int opt = 1;
    int result = setsockopt(tcpSocketFd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    
    if (result)
    {
        int errsv = errno;
        printf("ERROR: %s", strerror(errsv));
        return -5;
    }
    socklen_t len = sizeof(serverAddr);
    result = connect(tcpSocketFd, (struct sockaddr *) &serverAddr, len);
    if (result < 0) 
    {
        printf("Error: %i %s", errno, strerror(errno));
        return -1;
    }

    
    return tcpSocketFd;
}

int main(int argc, char * argv[])
{
    char tcpPort[1024];

    if (argc < 3) 
    {
        if (argc == 2) 
        {
            char readbuffer[1024];
            scanf("%s", readbuffer);
            strcpy(tcpPort, readbuffer);
        }
        else
        { 
            return -1;
        }
    } 
    else 
    {
        strcpy(tcpPort, argv[2]);

    }

    char * hostname = argv[1];
    
    char * nameBuffer = (char *) malloc(sizeof(char) * 256);
    int nameSize = 0;
    
    fgets(nameBuffer, 256, stdin);

    nameSize = strlen(nameBuffer);
    nameBuffer[nameSize - 1] = '\0';
    char * name = (char *) malloc(sizeof(char) * (nameSize + 1));
    strcpy(name, nameBuffer);
    free(nameBuffer); 
    printf("%s\n", name);

    char * helloMessage;
    size_t helloLength = 7 + nameSize;
    helloMessage = (char *) malloc(helloLength * sizeof(char));
    sprintf(helloMessage, "hello %s", name);


    int tcpsocketfd;
    if ((tcpsocketfd = connect_tcp(hostname, tcpPort)) < 0) 
    {
        return -1;
    }

    hsend(tcpsocketfd, helloMessage);
    printf("%i\n", (int) strlen(helloMessage));
    char readbuffer[1024];
    hread(tcpsocketfd, readbuffer, 1024);
    char command[1024];
    char param[1024];
    split_read(readbuffer, command, param);
    if (strcmp(command, "portUDP") != 0)
    {
        puts("expected portUDP");
    }
    printf("udp port is %s\n", param);

    int udpSocketFd;
    if ((udpSocketFd = connect_udp(hostname, param)) < 0) 
    {
        printf("Error %i\t%s\n", errno, strerror(errno));
        return -1;
    } 

    sockaddr_in cpy;
    socklen_t len = sizeof(cpy);

     getsockname(udpSocketFd, (struct sockaddr *)&cpy, &len);

    hsend(tcpsocketfd, "ready");
    start_udp(udpSocketFd, hostname, param);

    return 0;
}

