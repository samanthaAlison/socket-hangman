#include "game.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>


char * strtolower(char * str) 
{
    int length = strlen(str);

    char * out = (char *) malloc(sizeof(char) * (length + 1));
    
    char * strIter = str;
    length = 0;
    while (*strIter != '\0')
    {
        *out = (char) tolower((unsigned char) *strIter);
        out++;
        strIter++;
        length++;
    }
    return (out - length);
} 


Hangman::Hangman() {
    this->mWord = NULL;
    this->mWordWithBlanks = NULL;
    this->GetFileNewLineCount();
    time_t t;
    srand((unsigned) time(&t));
}

Hangman::~Hangman() {
    this->FreeWords();
}


void Hangman::FreeWords() 
{
    if (this->mWord != NULL) {
        free(this->mWord);
    }
    if (this->mWordWithBlanks != NULL) {
        free(this->mWordWithBlanks);
    }
}


int Hangman::GuessLeter(char guess)
{
    printf("Guess who?\n");
    int countCorrect = 0;
    char *word = this->mWord;
    char *wordWithBlanks = this->mWordWithBlanks;
    while (*word != '\0') 
    {
        printf("%c = %c?\n", guess, *word);
        if (*wordWithBlanks == '-' && guess == *word)
        {
            countCorrect++;
            *wordWithBlanks = guess;
            
        }
        wordWithBlanks++;
        word++;
    }
    if (countCorrect) 
    {
        this->mWin = (strcmp(this->mWord, this->mWordWithBlanks) == 0);
    }
    return countCorrect;
}

int Hangman::Guess(char * guess) 
{
    printf("%s s\n", guess);
    if (strlen(guess) == 1) 
    {
        return this->GuessLeter(tolower(guess[0]));
    }
    else 
    {
        char * lowerGuess = strtolower(guess);
        int result = this->GuessWord(strtolower(lowerGuess));
        free(lowerGuess);
        return result;
    }
    return 0;
}


int Hangman::GuessWord(char * guess) 
{

    if (strcmp(guess, this->mWord) == 0)
    {
        strcpy(this->mWordWithBlanks, this->mWord);
        this->mWin = 1;
        printf("%s\n", this->mWordWithBlanks);
        return (int) strlen(this->mWordWithBlanks);
    } 
    else 
    {
        return 0;
    }
}

int Hangman::GetFileNewLineCount() 
{
    int count = 0;
    FILE *fp;
    fp = fopen("words.txt", "r");
    char c = 'a';
    while (c != EOF) 
    {
        c = fgetc(fp);
        if (c == '\n') 
        {
            count++;   
        }
    }
    this->mFileNewLineCount = count;
    printf("The amount of \\n characters is %i.\n", count);
    return count;
}
void Hangman::NewGame(char * word = NULL) 
{   
    this->mWin = 0;
    this->FreeWords();
    if (word == NULL) 
    {
        this->PickWord();
    }
    else 
    {
        int wordLength = strlen(word);
        this->mWordWithBlanks = (char *) malloc(wordLength + 1);
        this->mWord = (char *) malloc(wordLength + 1);

        memset(this->mWordWithBlanks, (unsigned char) '-', wordLength);
        this->mWordWithBlanks[wordLength] = '\0';

        strcpy(this->mWord, word);
    }

}


void Hangman::PickWord() 
{
    int choice = rand() % (this->mFileNewLineCount + 1);
    FILE *fp;
    fp = fopen("words.txt", "r");
    char c = 'a';
    printf("choice: %i\n", choice);
    while (c != EOF) 
    {
        if (choice == 0) 
        {
            char * buffer = (char *) malloc(256);
            c = fgetc(fp);
            buffer[0] = c; 
            int wordLength = 1;
            while (c != EOF && c != '\n') 
            {
                c = fgetc(fp);
                buffer[wordLength] = c;
                wordLength++;
            }
            fseek(fp, -wordLength, SEEK_CUR);

            this->mWord = (char *) malloc(sizeof(char) * wordLength);
            this->mWordWithBlanks = (char *) malloc(sizeof(char) * wordLength);

            memcpy(this->mWord, buffer, wordLength - 1);
            this->mWord[wordLength - 1] = '\0';

            memset(this->mWordWithBlanks, '-', wordLength - 1);
            this->mWordWithBlanks[wordLength - 1] = '\0';

            printf("sanityCheck: %i\n", strlen(this->mWord));
            if (strlen(this->mWord) != wordLength - 1) {
                throw;
            }
            printf("word: %s\n", this->mWord);
            printf("mask: %s\n", this->mWordWithBlanks);
            free(buffer);
            printf("here\n");
            c = EOF;
        }
        else 
        {
          
            c = fgetc(fp);
            if (c == '\n') 
            {
                choice--;
            }
        }
    }

}

char * Hangman::GetWordWithBlank() 
{
    return this->mWordWithBlanks;
}

int Hangman::GetWin()
{
    return this->mWin;
}
