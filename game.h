#include <fstream>

class Hangman {
    

public:

    
    Hangman();
    ~Hangman();

    void NewGame(char * word);
    int Guess(char *);

    int GetWin();
    char * GetWordWithBlank();
private:
    char * GetWordFromFile();

    int mFileNewLineCount;
    int GetFileNewLineCount();
    int GuessLeter(char guess);
    int GuessWord(char * guess);
    char * mWord;
    char * mWordWithBlanks;

    void PickWord();

    void FreeWords();
    int mWin;


};