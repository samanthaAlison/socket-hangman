#include "game.h"
#include "shelp.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#define _SAM_TRACK_

int get_hello(int tcpSocketFd, char * name)
{
    char readBuffer[1024];

    int tcpRead = hread(tcpSocketFd, readBuffer, 1024);
    
    char command[1024];
    char param[1024];
    int result = split_read(readBuffer, command, param);
    if (result == 3) {
        strcpy(name, param);
        return 1;
    }
    return 0;
     
}

void get_sockaddr(int socketFd, struct sockaddr_in * addr)
{
    socklen_t len = sizeof(*addr);
    getsockname(socketFd, (struct sockaddr *) addr, &len);
}

in_port_t get_port(int socketFd)
{
    struct sockaddr_in addr;
    get_sockaddr(socketFd, &addr);

    return addr.sin_port;
}


int createUdpSocket(struct sockaddr_in * udpAddr)
{
    int udpSocketFd;
    // create udp port and send port number.
    if ((udpSocketFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        return -1;
    }
    
    struct sockaddr_in * initAddr = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));
    initAddr->sin_family = AF_INET;
    initAddr->sin_addr.s_addr = INADDR_ANY;
#ifdef _SAM_TRACK_
    initAddr->sin_port = htons(0);
#else
    initAddr->sin_port = htons(0);
#endif
    int opt = 1;
    
    int result = setsockopt(udpSocketFd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    
    if (result)
    {
        int errsv = errno;
        printf("ERROR: %s", strerror(errsv));
        return -5;
    }


    if (bind(udpSocketFd, (struct sockaddr *)initAddr, sizeof(*initAddr)) == -1)
    {
        return -2;
    } 


    socklen_t len = sizeof(udpAddr);
    getsockname(udpSocketFd, (struct sockaddr *) udpAddr, &len);



    free(initAddr);
    return udpSocketFd;
}

void new_game(Hangman *hangman, char * word)
{
    hangman->NewGame(word);
}

void udp_loop(int udpSocketFd, sockaddr_in * sendAddr, char * word, char * name) 
{
    Hangman hangman = Hangman();
    hangman.NewGame(word);
    int attempts = 5;

    bool session = true; 
    int game = 0;
    sockaddr_in recvAddr;
    socklen_t recvLen;
    recvLen = sizeof(recvAddr);
    memset(&recvAddr, '0', recvLen);
    //get_sockaddr(udpSocketFd, &recvAddr);

    sockaddr_in udpSendAddr;
    socklen_t sendLen = sizeof(udpSendAddr);
    memset(&udpSendAddr, '0', sizeof(udpSendAddr));

    //memcpy(&udpSendAddr, sendAddr, sendLen);
    udpSendAddr.sin_family = AF_INET;
    udpSendAddr.sin_addr = sendAddr->sin_addr;
    udpSendAddr.sin_port = get_port(udpSocketFd);
    printf("ip_addr: %s\n", inet_ntoa(udpSendAddr.sin_addr));

    printf("port number of tcp socket is: %i\n", (int) ntohs(udpSendAddr.sin_port));
    int sending = 0, receiving = 0;

    char readBuffer[1024];
    char sendBuffer[1024];
    while (session)
    {

        printf("game: %i\n", game );


        if (game == 1)
        {
            receiving = 1;
            sending = 0;
            recvLen = sizeof(recvAddr);

            hread_udp(udpSocketFd, readBuffer, 1024, 0, &recvAddr, &recvLen);
            
            puts(readBuffer);
            char command[1024];
            char param[1024];
            split_read(readBuffer, command, param);

            puts(command);
            if (strcmp(command, "ready") == 0)
            {
                new_game(&hangman, word);
                game = 2;
            } else  {

            }
        } 
        else if (game == 2)
        {
            recvLen = sizeof(recvAddr);
             receiving = 0;
            sprintf(sendBuffer, "stat: %s %i\n", hangman.GetWordWithBlank(), attempts);
            hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, recvLen);
            sending  = 1;
           
            receiving = 1;
            sending = 0;
            recvLen = sizeof(recvAddr);

            hread_udp(udpSocketFd, readBuffer, 1024, 0, &recvAddr, &recvLen);
            
            puts(readBuffer);
            char command[1024];
            char param[1024];
            split_read(readBuffer, command, param);

            puts(command);
            if (strcmp(command, "guess") == 0)
            {
                puts("it was a guess\n");
                if (!hangman.Guess(param))
                {
                    attempts--;
                    if (attempts <= 0) 
                    {
                        game = 0;
                        sprintf(sendBuffer, "end you_lose!");
                        hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, recvLen);
                    }
                }
                if (hangman.GetWin() == 1)
                {
                    
                    game = 0;
                    sprintf(sendBuffer, "end you_win!");
                    hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, recvLen);
                }
            } 
            else if (strcmp (command, "end") == 0)
            {
            
                game = 0;
                sprintf(sendBuffer, "end you_quit");
                hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, recvLen);

            } 
            else if (strcmp (command, "bye") == 0)
            {
                sprintf(sendBuffer, "bye");
                hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, recvLen);
                session = false;
            }
            else 
            {
                puts("invalid command");
                sprintf(sendBuffer, "na  type_ready_for_new_game_or_bye_to_exit");
                game = 0;
                hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, recvLen);
            }
        }
        else if (game == 0)
        {
            hread_udp(udpSocketFd, readBuffer, 1024, 0, &recvAddr, &recvLen);
            puts(readBuffer);
            char command[1024];
            char param[1024];
            split_read(readBuffer, command, param);

            if (strcmp(command, "ready") == 0)
            {
                //sprintf(sendBuffer, "instr Well_howdy,_%s._My_name_is_man_and_we\'re_going_to_be_playing_hangman._I_have_a_word,_and_I\'ll_show_you_the_blanks_that_represent_each_letter._You_can_guess_letters_of_the_word_or_the_whole_word._You_can_only_make_5_incorrect_guesses_before_you_lose._To_guess,_enter_\"guess_#\"_where_\"#\"_is_your_guess._To_end_a_game_enter_\"end\"._To_start_a_new_game_after_that_\"ready\"._To_leave_the_game_and_not_start_a_new_one_\"bye\".", name);
                //hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, recvLen);
   
                new_game(&hangman, word);
                game = 2;
            }
            else  if (strcmp(command, "bye") == 0) 
            {
                sprintf(sendBuffer, "bye");
                hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, recvLen);
                session = false;
            }
            else 
            {
                sprintf(sendBuffer, "na  type_ready_for_new_game_or_bye_to_exit");
                hsend_udp(udpSocketFd, sendBuffer, 0, &recvAddr, recvLen);
            }
        }
    }
    
    
    
    


}

int main (int argc, char *argv[]) 
{


    char * word = NULL;

    const char * randOpt = (char*) "-r";
    if (argc >= 1) 
    {
        
        if (strcmp(argv[1], randOpt) != 0)
        {
            int length = strlen(argv[1]);
            word = (char *) malloc(sizeof(char) * (length + 1));
            strcpy(word, argv[1]);
        }

        
    }
    while (true)
    {
        struct sockaddr_in * tcpAddr, udpAddr;
        int tcpSocketFd, tcpAcceptFd, udpSocketFd;
        int opt = 1;
        
        if ((tcpSocketFd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        {
            return -1;
        }
        tcpAddr = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));
        tcpAddr->sin_family = AF_INET;
        tcpAddr->sin_addr.s_addr = INADDR_ANY;
    #ifdef _SAM_TRACK_
        tcpAddr->sin_port = htons(47926);
    #else
        initAddr->sin_port = htons(0);
    #endif
        
        int result = setsockopt(tcpSocketFd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
        
        if (result)
        {
            int errsv = errno;
            printf("ERROR: %s", strerror(errsv));
            return -5;
        }


        if (bind(tcpSocketFd, (struct sockaddr *)tcpAddr, sizeof(*tcpAddr)) == -1)
        {
            return -2;
        } 

        if (listen(tcpSocketFd, 50) == -1)
        {
            return -3;
        }


        sockaddr_in cpy, tcpAcptAddr;
        socklen_t len = sizeof(cpy);
        getsockname(tcpSocketFd, (struct sockaddr *)&cpy, &len);
        printf("ip_addr: %s", inet_ntoa(cpy.sin_addr));

        printf("port number of tcp socket is: %i\n", (int) ntohs(get_port(tcpSocketFd)));


        if ((tcpAcceptFd = accept(tcpSocketFd, (struct sockaddr *) &tcpAcptAddr, &len)) < 0)
        {
            printf("Error: %s\n", strerror(errno));
            return -4;
        }

        

        char name[1024];

        if (get_hello(tcpAcceptFd, name) == 0) {
            puts("Error getting name");
            return -6;
        }
        printf("name: %s\n", name);

        // create udpSocket
        if  ((udpSocketFd = createUdpSocket(&udpAddr)) < 0) {
            printf("Error creating udp socket %i\n", udpSocketFd);
            return -8;
        }
        printf("port number of udp socket is: %i\n", (int) ntohs(get_port(udpSocketFd)));

        char writebuffer[1024];

        sprintf(writebuffer, "portUDP %i", (int) ntohs(get_port(udpSocketFd)));

        hsend(tcpAcceptFd, writebuffer);

        char readBuffer[1024];
        hread(tcpAcceptFd, readBuffer, 1024);

        puts(readBuffer);
        if (strcmp(readBuffer, "ready") != 0) 
        {
            puts("expected ready\n");
            return -8;
        }

        udp_loop(udpSocketFd, &tcpAcptAddr, word, name );

        close(udpSocketFd);
        close(tcpAcceptFd);
        close(tcpSocketFd);
    }
    return 0;
}