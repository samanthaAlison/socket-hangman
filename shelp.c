#pragma once
#include "shelp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>

int hsend(int fd, char * message) 
{
    char * fullMessage = (char *) malloc(sizeof(char) * (strlen(message) + 10));
    sprintf(fullMessage, "%i %s", (int) strlen(message), message);
    int result = send(fd, fullMessage, strlen(fullMessage), 0); 
    if (errno)
        printf("Error %s\n", strerror(errno));
    free(fullMessage);
    return result;
}
int hsend_udp(int fd, char * message, int flags, struct sockaddr_in *dest_addr, socklen_t addrlen)
{
    char * fullMessage = (char *) malloc(sizeof(char) * (strlen(message) + 10));
    sprintf(fullMessage, "%i %s", (int) strlen(message), message);
    int result = sendto(fd, fullMessage, strlen(fullMessage), flags, (struct sockaddr *) dest_addr, addrlen);
    if (errno)
        printf("Error: %i\t %s\n", errno, strerror(errno));
    puts(fullMessage);
    free(fullMessage);
    return result;
}

int hread(int fd, char * message, int maxSize)
{
    char preMessage[maxSize];
    int result = read(fd, preMessage, maxSize);
    char cpy[maxSize];
    memcpy(cpy, preMessage, maxSize);
    int prefix = 1;
    for (char * cpyIter = cpy; *cpyIter != ' '; ++cpyIter)
    {
        prefix++;
    }
    char * lengthString = strtok(cpy, " ");
    int len = 0;
    if (lengthString) 
    {
        len = atoi(lengthString);
    }
    memcpy (message, preMessage + prefix, len);
    message[len] = '\0';
    return result;
}

int hread_udp(int fd, char * message, int maxSize, int flags,  struct sockaddr_in *src_addr, socklen_t *addrlen)
{
    char preMessage[maxSize];
    int result = recvfrom(fd, preMessage, maxSize, flags, (struct sockaddr *) src_addr, addrlen);

    char cpy[maxSize];
    memcpy(cpy, preMessage, maxSize);
    int prefix = 1;
    for (char * cpyIter = cpy; *cpyIter != ' '; ++cpyIter)
    {
        prefix++;
    }
    char * lengthString = strtok(cpy, " ");
    int len = 0;
    if (lengthString) 
    {
        len = atoi(lengthString);
    }
    memcpy (message, preMessage + prefix, len);
    message[len] = '\0';
    return result;


}

int split_read(char * read, char * command, char * param) 
{
    
    char * token = strtok(read, " ");
    if (token) {
        strcpy(command, token);
        token = strtok(NULL, " ");
        if (token) {
            strcpy(param, token);
            return 3;
        }
        return 1;
    }
    return 0;
}
