#pragma once
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
int hsend(int fd, char * message);
int hsend_udp(int fd, char * message, int flags, struct sockaddr_in *dest_addr, socklen_t addrlen);
int hread(int fd, char * message, int maxSize);
int hread_udp(int fd, char * message, int maxSize, int flags,  struct sockaddr_in *src_addr, socklen_t *addrlen);
int split_read(char * read, char * command, char * param);
