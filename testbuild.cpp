#include "game.h"
#include <stdio.h>
#include <ctype.h>
#include <iostream>
int main(int argc, char** argv)
{
    printf("This is a test %i", 2);

    std::cout << "Hello"<<std::endl;
    Hangman * hangman = new Hangman();
    for (int i = 0; i < 100; i++) 
    {
        printf("\n*******\n\n");
        hangman->NewGame((char *)"word");
        
        while(hangman->GetWin() == 0) 
        {
            printf("%s\n", hangman->GetWordWithBlank());
            char * inputBuffer = (char *)malloc(sizeof(char) * 128);
            scanf("%s", inputBuffer);
            
            int inputSize = strlen(inputBuffer);

            char end = inputBuffer[inputSize - 1];
            while (inputSize >= 0 && (end == ' ' || end == '\t')) 
            {
                inputSize--;
                end = inputBuffer[inputSize - 1];
            }  
            if (inputSize > 0) 
            {
                char * guess = (char *) malloc(sizeof(char) * (inputSize + 1));
                memcpy(guess, inputBuffer, inputSize);
                guess[inputSize] = '\0';
                hangman->Guess(guess);
            }
        }
    }
} 